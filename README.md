![Sand Dune](/dune.jpg "Sand Dune")

![Network Topology](/topology.png "Network Topology")

This example has been adapted from [mininet_ospf_bgp](https://github.com/edwinsc/mininet_ospf_bgp).


# Start with a clean Ubuntu 18.04 LTS machine

We are assuming there is a user mininet defined in this machine, with
password-less sudo access.

You can create such a VM on Azure using the
[Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
and the
[azure-spinup](/azure-spinup) script shipped in this repository.

# Install pre-requisites

- As root, install the pre-requisites:
```bash
apt-get -qy update && \
apt-get -qy dist-upgrade && \
apt-get -qy install mininet iputils-ping tmux python-termcolor openvswitch-testcontroller quagga git vim-nox python-setuptools python-all-dev flex bison traceroute && \
mkdir /run/quagga && \
chown quagga /run/quagga && \
ln -s /usr/bin/ovs-testcontroller /usr/local/bin/ovs-controller
```

- As mininet, install mininet
```
git clone git://github.com/mininet/mininet && \
./mininet/util/install.sh -fnv && \
cd $HOME && \
git clone https://gitlab.com/graphsIB/ospf.git ${HOME}/ospf
```

- As root, reboot, in case the massive upgrade introduced e.g. kernel updates
```bash
reboot
```

# Start the network simulation

Either use tmux or start several SSH connections to the VM,
as the next step will keep a terminal busy.  
As mininet,
```bash
tmux
cd ospf/mininet_ospf_bgp/ospf_bgp
sudo mkdir /var/run/quagga
sudo chown quagga /var/run/quagga
sudo python ./start.py
```

# Interact with Quagga

![Topology with Networks](/ospf.png "Topology with Networks")


You can then enter the network namespace of ospfd and zebra
in order to interact with them:
```bash
sudo nsenter -n -t $(cat /tmp/ospfd-r010_4.pid) telnet localhost ospfd
sudo nsenter -n -t $(cat /tmp/zebra-r010_4.pid) telnet localhost zebra
```

# Understanding end-to-end connectivity

## Reaching non-directly-connected hosts

```
mininet> h010_41 ping h010_21
```

## Zebra

As the core daemon for the route to `h010_21` (we learned its IP address above):
```bash
$ sudo nsenter -n -t $(cat /tmp/zebra-r010_4.pid) telnet localhost zebra
r010_4> enable
# Password is en
r010_4# en
# Password is en 
r010_4# sh ip route 10.2.0.2
Routing entry for 10.2.0.0/24
  Known via "ospf", distance 110, metric 20, tag 0, vrf 0, best, fib
  Last update 00:13:22 ago
  >* 10.255.0.9, via r010_4-eth1
```

## OSPF

Ask the OSPF daemon for its OSPF database, then observe its LSA.

```bash
$ sudo nsenter -n -t $(cat /tmp/ospfd-r010_4.pid) telnet localhost ospfd
# Password is en
r010_4> enable
r010_4# sh ip ospf database

       OSPF Router with ID (10.10.0.4)

                Router Link States (Area 0.0.0.0)

Link ID         ADV Router      Age  Seq#       CkSum  Link count
10.10.0.1       10.10.0.1        876 0x8000000a 0xb2d5 3
10.10.0.2       10.10.0.2        877 0x80000009 0x613c 3
[...]
r010_4# sh ip ospf database router

       OSPF Router with ID (10.10.0.4)


                Router Link States (Area 0.0.0.0)

  LS age: 1337
  Options: 0x2  : *|-|-|-|-|-|E|-
  LS Flags: 0x6
  Flags: 0x2 : ASBR
  LS Type: router-LSA
  Link State ID: 10.10.0.1
  Advertising Router: 10.10.0.1
  LS Seq Number: 8000000a
```
